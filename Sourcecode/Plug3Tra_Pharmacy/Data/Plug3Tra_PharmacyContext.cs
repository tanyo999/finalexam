using Microsoft.AspNetCore.Identity.EntityFrameworkCore; 
using Plug3Tra_Pharmacy.Models;
using Microsoft.EntityFrameworkCore;
namespace Plug3Tra_Pharmacy.Data
{
 public class Plug3Tra_PharmacyContext :  IdentityDbContext<NewsUser> 
 {
 public DbSet<News> newsList { get; set; }
 public DbSet<NewsCategory> NewsCategory { get; set; }

 protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
 {
 optionsBuilder.UseSqlite(@"Data source=Plug3Tra_Pharmacy.db");
 }
 }
}