using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Plug3Tra_Pharmacy.Data;
using Plug3Tra_Pharmacy.Models;

[assembly: HostingStartup(typeof(Plug3Tra_Pharmacy.Areas.Identity.IdentityHostingStartup))]
namespace Plug3Tra_Pharmacy.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}