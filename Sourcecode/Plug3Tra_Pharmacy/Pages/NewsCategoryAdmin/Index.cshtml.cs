using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Plug3Tra_Pharmacy.Data;
using Plug3Tra_Pharmacy.Models;

namespace Plug3Tra_Pharmacy.Pages.NewsCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext _context;

        public IndexModel(Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext context)
        {
            _context = context;
        }

        public IList<NewsCategory> NewsCategory { get;set; }

        public async Task OnGetAsync()
        {
            NewsCategory = await _context.NewsCategory.ToListAsync();
        }
    }
}
