using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Plug3Tra_Pharmacy.Data;
using Plug3Tra_Pharmacy.Models;

namespace Plug3Tra_Pharmacy.Pages.NewsCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext _context;

        public EditModel(Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsCategory NewsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsCategory = await _context.NewsCategory.FirstOrDefaultAsync(m => m.NewsCategoryID == id);

            if (NewsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(NewsCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsCategoryExists(NewsCategory.NewsCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewsCategoryExists(int id)
        {
            return _context.NewsCategory.Any(e => e.NewsCategoryID == id);
        }
    }
}
