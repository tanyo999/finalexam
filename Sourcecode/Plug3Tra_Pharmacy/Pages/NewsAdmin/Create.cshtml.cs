using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plug3Tra_Pharmacy.Data;
using Plug3Tra_Pharmacy.Models;

namespace Plug3Tra_Pharmacy.Pages.NewsAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext _context;

        public CreateModel(Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsCategoryID"] = new SelectList(_context.NewsCategory, "NewsCategoryID", "ShortName");
            return Page();
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsList.Add(News);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}