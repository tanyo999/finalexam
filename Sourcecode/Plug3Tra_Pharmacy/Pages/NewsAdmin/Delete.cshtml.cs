using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Plug3Tra_Pharmacy.Data;
using Plug3Tra_Pharmacy.Models;

namespace Plug3Tra_Pharmacy.Pages.NewsAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext _context;

        public DeleteModel(Plug3Tra_Pharmacy.Data.Plug3Tra_PharmacyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public News News { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.newsList
                .Include(n => n.NewsCat).FirstOrDefaultAsync(m => m.NewsID == id);

            if (News == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            News = await _context.newsList.FindAsync(id);

            if (News != null)
            {
                _context.newsList.Remove(News);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
